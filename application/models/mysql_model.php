<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mysql_model extends CI_Model{

  function __construct()
  {
    parent::__construct();
    $this->db2 = $this->load->database('mysql', TRUE);
  }

  public function connect(){
    $cmd = "SELECT * FROM tb_user";
		$query = $this->db2->query($cmd)->num_rows();
    return $query;
  }
}
