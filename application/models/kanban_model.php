<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kanban_model extends CI_Model{

  function __construct()
  {
    parent::__construct();
    // $this->db2 = $this->load->database('mysql', TRUE);
  }

  // public function connect(){
  //   $cmd = "SELECT * FROM tb_user";
	// 	$query = $this->db2->query($cmd)->num_rows();
  //   return $query;
  // }
  //
  public function hr_staff(){
    // $db_obj = $this->load->database();
		$cmd = "SELECT * FROM hr_staff";
		$query = $this->db->query($cmd)->num_rows();
    return $query;
  }

//--//*** จำนวน WL และ คงเหลือ ***//--
  public function count_WL(){
    $cmd = "select * from hr_staff where site_code = '60LBDL0145'";
    $query = $this->db->query($cmd);
    $WLcount = $query->num_rows();

    $cmd1 = "select * from st_inform_detail a LEFT JOIN hr_staff b ON a.staff_id = b.staff_id and a.site_code = b.site_code
    where a.site_code = '60LBDL0145' and a.inform_no like 'SQ%' and CAST(a.begin_date AS DATE) = '2017-06-01'";
    $query1 = $this->db->query($cmd1);
    $WLcount1 = $query1->num_rows();

    $T_WL = $WLcount - $WLcount1;
    $callback = array("WLcount"=>$WLcount, "WLuse"=>$WLcount1, "T_WL"=>$T_WL);
    return $callback;
  }

//--//*** จำนวน ใบสมัคร ***//--
  public function count_register(){
    $cmd = "select count(*) from hr_recruitment where CAST(application_date AS DATE) = '2017-06-01'";
    $query = $this->db->query($cmd);
    return $query;
  }

//--//*** ใบขอส่งตัวคงค้าง ***//--
  public function request_hold(){
    $cmd = "select COUNT(inform_qty) as request_hold from st_request_head WHERE request_no like 'SQ60%' AND request_qty <> inform_qty
    AND CAST(request_date AS DATE) = '2017-06-01'";
    $query = $this->db->query($cmd);
    $rows = $query->num_rows();
    return $rows;
  }

//--//*** ใบขอแทนงานและคงค้าง ***//--
  public function replace_hold(){
    $cmd = " select * from st_replace_head a LEFT JOIN st_replace_staff b on a.replace_no = b.replace_no
      WHERE a.replace_no like 'SQ60%' AND CAST(b.abs_date AS DATE) = '2017-06-01' AND a.status = 'A' ";
    $query = $this->db->query($cmd);
    $nums = $query->num_rows();
    $rows = $query->result_array();

    $cmd .= " and b.dd <> b.dd_replace";
    $query1 = $this->db->query($cmd);
    $nums1 = $query->num_rows();
    $rows1 = $query->result_array();
    $callback = array("rq" => $nums, "rq_rows" => $rows, "rp",$nums1, "rp_rows" => $rows1);
    return $rows;
  }

//--//*** ประมาณการค่าปรับและลดมูลค่า ***//--
  public function deduct(){
    $cmd = "select SUM(b.abs_customer_deduct) as abs_customer_deduct, SUM(b.abs_customer_fine) as abs_customer_fine
            from st_request_head a LEFT JOIN st_request_p3 b ON a.request_no = b.request_no
            WHERE a.request_no like 'SQ60%' AND a.request_qty <> a.inform_qty
             AND CAST(a.request_date AS DATE) = '2017-06-01'";
    $query = $this->db->query($cmd);
    $rows = $query->result_array();
    $cusdeduct = $rows[0]["abs_customer_deduct"];
    $custfine = $rows[0]["abs_customer_fine"];
    $all_deduct = $cusdeduct + $custfine;
    $callback = array("cusdeduct"=>$cusdeduct, "custfine"=>$custfine, "all_deduct"=>$all_deduct);
    return $callback;
  }

//--//*** ประมาณการต้องการคน และ ส่งแล้ว ต่อปี ***//--
  public function m_request_inform(){
    $cmd=" select sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 01 then request_qty else 0 end)'request_m1',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 02 then request_qty else 0 end)'request_m2',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 03 then request_qty else 0 end)'request_m3',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 04 then request_qty else 0 end)'request_m4',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 05 then request_qty else 0 end)'request_m5',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 06 then request_qty else 0 end)'request_m6',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 07 then request_qty else 0 end)'request_m7',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 08 then request_qty else 0  end)'request_m8',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 09 then request_qty else 0 end)'request_m9',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 10 then request_qty else 0 end)'request_m10',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 11 then request_qty else 0 end)'request_m11',
           sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 12 then request_qty else 0 end)'request_m12'
            from st_request_head where request_no like 'SQ%'";
    $query = $this->db->query($cmd);
    $rows = $query->result_array();

    $cmd1 = "select sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 01 then request_qty else 0 end)'request_m1',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 02 then request_qty else 0 end)'request_m2',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 03 then request_qty else 0 end)'request_m3',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 04 then request_qty else 0 end)'request_m4',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 05 then request_qty else 0 end)'request_m5',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 06 then request_qty else 0 end)'request_m6',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 07 then request_qty else 0 end)'request_m7',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 08 then request_qty else 0 end)'request_m8',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 09 then request_qty else 0 end)'request_m9',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 10 then request_qty else 0 end)'request_m10',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 11 then request_qty else 0 end)'request_m11',
             sum(case when YEAR(request_date)= 2017 AND MONTH(request_date) = 12 then request_qty else 0 end)'request_m12'
            from st_request_head where request_no like 'SQ%' AND (is_inform_all = 'Y' OR is_close = 'Y')";
      $query1 = $this->db->query($cmd1);
      $rows1 = $query->result_array();
      $callback = array("m_request"=>$rows, "m_inform"=>$rows1);
    return $callback;
  }

//--//*** ใบขอจริง ***//--
  public function request_emp(){
    $cmd="select COUNT(*) from st_request_head a where a.status='A' and a.is_stop='N'
    and a.is_inform_all='N' and a.request_no like 'SQ%' and CAST(request_date AS DATE) = '2017-09-08'";
    $query = $this->db->query($cmd);
    $rows = $query->num_rows();
    return $rows;
  }

//--//*** w_sheet_st2_kanban ใบขอจริง + รายละเอียด ***//--
  public function request_emp_detail(){
    $cmd = "SELECT
            a.request_no'request_no',
            a.site_code'site_code',
            s.site_name'site_name',
            (select z.staff_title_name from hr_ms_staff_title z where a.staff_title_code=z.staff_title_code)'title_name',
            (select z.job_description_name from hr_ms_job_description_1 z where a.job_description_code_1=z.job_description_code_1)'job_name1',
            a.job_description_code_2'job2_code',
            a.want_date_from 'want_date1',
            a.want_date_to 'want_date2',
            a.request_qty'request_qty',
            a.inform_qty 'inform_qty',
            coalesce((select top 1 z.payment_rate from st_request_p3_rate z where a.request_no=z.request_no and z.payment_rate > 0 order by seq  ),0)'rate',
            0 'want_date1_edit',
            0 'want_date2_edit'

            FROM st_request_head a, ms_site s
            where a.site_code=s.site_code
            and a.status='A'
            and a.is_stop='N'
            and a.is_inform_all='N'
            and s.department_code not in ('CR', 'LM', 'DS')";
            $query = $this->db->query($cmd);
            $rows = $query->result_array();
            $count = $query->num_rows();
            $callback = array("data"=>$rows, "count"=>$count);
            return $callback;
  }

//--//*** w_sheet_st2_kanban ใบขอแทนงาน + รายละเอียด ***//--
  public function request_temp_emp(){
    $cmd = "select
        a.replace_no,
        s.site_code + '/' + s.site_name 'site',
        b.begin_date'begin_date',
        b.end_date'end_date',
        b.begin_day'begin_day',
        b.end_day'end_day',
        b.day_no'day_no',
        c.abs_date 'abs_date',
        c.abs_day 'abs_day',
        c.dd 'dd_off',
        c.dd_replace'dd_on',
        case when b.absence_type_code = '007' then (select z1.fname from st_replace_resign z,hr_staff z1 where z.staff_id = z1.staff_id and z.replace_no = a.replace_no)
        else coalesce((select z.fname from hr_staff z where a.absence_id=z.staff_id),'ไม่มี') end'absence_name',
        (select z.absence_type_name from st_ms_absence_type z where b.absence_type_code = z.absence_type_code)'absence_type',
        coalesce((select z.staff_title_name from hr_ms_staff_title z where a.staff_title_code=z.staff_title_code),'*')'title_name',
        (select z.job_description_name from hr_ms_job_description_1 z where a.job_description_code_1=z.job_description_code_1)'job_name1',
        (select sum(z.dd_replace) from st_replace_staff z where z.replace_no = a.replace_no and z.abs_date between b.begin_date and b.end_date and z.is_temp = a.is_temp)'replace_day',
        coalesce((select space(1) + z1.fname from st_replace_staff z,hr_recruitment z1 where z.replace_id = z1.citizen_id and z.replace_no = a.replace_no and z.is_temp=c.is_temp and z.abs_date =c.abs_date),'-')'replace_staff',
        0'begin_date_edit',
        0'end_date_edit',
        0'abs_date_edit'
      from st_replace_head a, st_replace_type b, st_replace_staff c, ms_site s
      where a.replace_no=	b.replace_no
      and a.replace_No=c.replace_no
      and a.is_temp=c.is_temp
      and a.is_temp=b.is_temp
      and a.site_code=s.site_code
      and a.status = 'A'
      and a.is_nreplace = 'N'
      and s.department_code not in ('CR', 'LM', 'DS')
      and ( b.begin_date >= convert(varchar(11),getdate(),112) or b.end_date >= convert(varchar(11), getdate(), 112) )
      ";
      $query = $this->db->query($cmd);
      $rows = $query->result_array();
      $count = $query->num_rows();
      $callback = array("data"=>$rows, "count"=>$count);
      return $callback;
  }

//--//*** สถิติการลา  xsp_r_st2_replace_stat_yy ***//--
  public function vacation_static(){
    $cmd = "Declare
        @as_site_code1 varchar(10)='59LBDL0085',
        @as_site_code2 varchar(10)='Z'
         select
        A.absence_id'absence_id',
        A.site_code'site_code',
        A.group_code'group_code',
        A.team_code'team_code',
        A.contract_no'contract_no',
        A.contract_edition'contract_edition',
        A.contract_seq'contract_seq',
        S.contract_begin'contract_begin',
        S.contract_expired'contract_expired',
        (select Z.sick_day from st_site_contract_p2 Z where Z.contract_no = A.contract_no and Z.contract_edition = A.contract_edition and Z.contract_seq = A.contract_seq)'sick_day',
        (select Z.holiday_day from st_site_contract_p2 Z where Z.contract_no = A.contract_no and Z.contract_edition = A.contract_edition and Z.contract_seq = A.contract_seq)'holiday_day',
        (select Z.business_day from st_site_contract_p2 Z where Z.contract_no = A.contract_no and Z.contract_edition = A.contract_edition and Z.contract_seq = A.contract_seq)'business_day',
        sum(case when C.absence_type_code = '001' and year(S.contract_begin) = year(B.abs_date) then B.dd else 0 end)'abs_001yy1',
        sum(case when C.absence_type_code = '002' and year(S.contract_begin) = year(B.abs_date) then B.dd else 0 end)'abs_002yy1',
        sum(case when C.absence_type_code = '003' and year(S.contract_begin) = year(B.abs_date) then B.dd else 0 end)'abs_003yy1',
        sum(case when C.absence_type_code = '004' and year(S.contract_begin) = year(B.abs_date) then B.dd else 0 end)'abs_004yy1',
        sum(case when C.absence_type_code = '005' and year(S.contract_begin) = year(B.abs_date) then B.dd else 0 end)'abs_005yy1',

        sum(case when C.absence_type_code = '001' and year(S.contract_begin)+ 1 = year(B.abs_date) then B.dd else 0 end)'abs_001yy2',
        sum(case when C.absence_type_code = '002' and year(S.contract_begin)+ 1 = year(B.abs_date) then B.dd else 0 end)'abs_002yy2',
        sum(case when C.absence_type_code = '003' and year(S.contract_begin)+ 1 = year(B.abs_date) then B.dd else 0 end)'abs_003yy2',
        sum(case when C.absence_type_code = '004' and year(S.contract_begin)+ 1 = year(B.abs_date) then B.dd else 0 end)'abs_004yy2',
        sum(case when C.absence_type_code = '005' and year(S.contract_begin)+ 1 = year(B.abs_date) then B.dd else 0 end)'abs_005yy2',

        sum(case when C.absence_type_code = '001' and year(S.contract_begin)+ 2 = year(B.abs_date) then B.dd else 0 end)'abs_001yy3',
        sum(case when C.absence_type_code = '002' and year(S.contract_begin)+ 2 = year(B.abs_date) then B.dd else 0 end)'abs_002yy3',
        sum(case when C.absence_type_code = '003' and year(S.contract_begin)+ 2 = year(B.abs_date) then B.dd else 0 end)'abs_003yy3',
        sum(case when C.absence_type_code = '004' and year(S.contract_begin)+ 2 = year(B.abs_date) then B.dd else 0 end)'abs_004yy3',
        sum(case when C.absence_type_code = '005' and year(S.contract_begin)+ 2 = year(B.abs_date) then B.dd else 0 end)'abs_005yy3',

        sum(case when C.absence_type_code = '001' and year(S.contract_begin)+ 3 = year(B.abs_date) then B.dd else 0 end)'abs_001yy4',
        sum(case when C.absence_type_code = '002' and year(S.contract_begin)+ 3 = year(B.abs_date) then B.dd else 0 end)'abs_002yy4',
        sum(case when C.absence_type_code = '003' and year(S.contract_begin)+ 3 = year(B.abs_date) then B.dd else 0 end)'abs_003yy4',
        sum(case when C.absence_type_code = '004' and year(S.contract_begin)+ 3 = year(B.abs_date) then B.dd else 0 end)'abs_004yy4',
        sum(case when C.absence_type_code = '005' and year(S.contract_begin)+ 3 = year(B.abs_date) then B.dd else 0 end)'abs_005yy4',

        sum(case when C.absence_type_code = '001' then B.dd else 0 end)'abs_001sum',
        sum(case when C.absence_type_code = '002' then B.dd else 0 end)'abs_002sum',
        sum(case when C.absence_type_code = '003' then B.dd else 0 end)'abs_003sum',
        sum(case when C.absence_type_code = '004' then B.dd else 0 end)'abs_004sum',
        sum(case when C.absence_type_code = '005' then B.dd else 0 end)'abs_005sum'

        from st_replace_head A, st_replace_staff B, st_replace_type C, ms_site S
        where A.status = 'A'
        and A.site_code = S.site_code
        and A.replace_no = B.replace_no
        and A.is_temp = B.is_temp
        and B.replace_no = C.replace_no
        and B.is_temp = C.is_temp
        and A.is_temp = 'N'
        and A.site_code between @as_site_code1 and @as_site_code2
        group by A.site_code, A.group_code, A.team_code, A.absence_id, A.contract_no, A.contract_edition, A.contract_seq, S.contract_begin, S.contract_expired
      ";
      $query = $this->db->query($cmd);
      $rows = $query->result_array();
      $count = $query->num_rows();
      $callback = array("data"=>$rows, "count"=>$count);
      return $callback;
  }
}
